FROM alpine
RUN echo Update Package Management.
RUN apk update
RUN apk upgrade --available
RUN sync && lbu ci
RUN echo Install Basic Softwares.
RUN apk add openssh-server apk-tools bash curl perl gcc g++ make bison flex lua lynx irssi
# 2	Install & Configure Basic Firewall.
RUN apk add ip6tables && apk add -u awall
# 2.1 Load Modules Default.
RUN modprobe ip_tables
RUN modprobe iptable_nat
# 2.2 Make the firewall autostart at boot and autoload.
RUN rc-update add iptables
RUN rc-update add ip6tables
